package com.example.demo.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ProductoDao;
import com.example.demo.domain.Producto;

@RestController
@RequestMapping(value = "/api/productos")
@CrossOrigin(origins = "*")
public class ProductoCtrl {

	@Autowired
	ProductoDao productoDao;

	@GetMapping
	Page<Producto> get(@RequestParam(required = false) String d, Pageable pageable) {
		if (!StringUtils.isEmpty(d))
			return productoDao.findByDescripcionContainingIgnoreCase(d, pageable);
		return productoDao.findAll(pageable);
	}

	@GetMapping(value = "/{id}")
	Producto get(@PathVariable("id") Long id) {
		return productoDao.getOne(id);
	}

	@PostMapping
	Producto get(@RequestBody Producto producto) {
		return productoDao.save(producto);
	}

	@PutMapping(value = "/{id}")
	Producto get(@PathVariable("id") Long id, @RequestBody Producto producto) {
		return productoDao.save(producto);
	}
}
