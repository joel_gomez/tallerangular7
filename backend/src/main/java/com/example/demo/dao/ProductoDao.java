package com.example.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.domain.Producto;

public interface ProductoDao extends JpaRepository<Producto, Long> {

	Page<Producto> findByDescripcionContainingIgnoreCase(String descripcion, Pageable pageable);

}
