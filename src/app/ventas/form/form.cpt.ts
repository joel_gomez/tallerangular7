import { HttpClient } from '@angular/common/http';
import { Component } from "@angular/core";
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'venta-form',
    templateUrl: './form.cpt.html'
})
export class VentaFormCtp {

    formulario: FormGroup;

    constructor(
        private http: HttpClient) {
    }

    ngOnInit() {
        this.formulario = new FormGroup({
            serie: new FormControl(),
            numeracion: new FormControl(),
            fecha: new FormControl(),

            cliente: new FormGroup({
                tipoDocumento: new FormControl(),
                nroDocumento: new FormControl(),
                nombre: new FormControl(),
            }),
            // linea: this.crearLinea(),//FormGroup
            lineas: new FormArray([
                this.crearLinea() // FG
            ]),
            total: new FormControl()
        });

        this.formulario.controls.lineas.valueChanges
            .subscribe(lineas => {
                const total = lineas.map(linea => {
                    return linea.total;
                }).reduce((t, e) => t + e);
                this.formulario.controls.total.setValue(total);
            })


    }

    crearLinea() {
        const formGroupLinea = new FormGroup({
            producto: new FormControl(),
            precio: new FormControl(),
            cantidad: new FormControl(),
            total: new FormControl(),
        });
        combineLatest(
            formGroupLinea.controls.precio.valueChanges,
            formGroupLinea.controls.cantidad.valueChanges

        ).subscribe((values: any[]) => {
            const precio = values[0];
            const cantidad = values[1];
            formGroupLinea.controls.total.setValue(precio * cantidad);
        });
        return formGroupLinea;
    }

    agregar() {
        const lineas = this.formulario.controls.lineas as FormArray;
        lineas.push(
            this.crearLinea()
        )
    }

    eliminarLinea(indice: number) {
        const lineas = this.formulario.controls.lineas as FormArray;
        lineas.removeAt(indice);
    }
    save() {
    }

}

