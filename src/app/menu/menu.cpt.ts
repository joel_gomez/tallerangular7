import { Component } from '@angular/core';
import { template } from '@angular/core/src/render3';


@Component({
    selector: 'menu-cpt',
    templateUrl: './menu.html'
})
export class MenuCpt {

    opciones = [
        {
            name: 'Producto',
            path: 'productos'
        },
        {
            name: 'Cliente',
            path: 'clientes'

        },
        {
            name: 'Venta',
            path: 'ventas'

        }
    ]


}