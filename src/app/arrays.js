var linea1= {
    producto:{
        id:3,
        nombre:'ddd',
        precio:3
    },
    cantidad:40,
    gratis:true
};
var linea2= {
    producto:{
        id:4,
        nombre:'dccccccdd',
        precio:30
    },
    cantidad:4,
    gratis:false
};
var linea3= {
    producto:{
        id:6,
        nombre:'dddccccccc',
        precio:2
    },
    cantidad:400,
    gratis:false
};
var linea4= {
    producto:{
        id:300,
        nombre:'ddccccccccd',
        precio:22
    },
    cantidad:42,
    gratis:true
};

var lineas=[linea1,linea2,linea3,linea4];

lineas.filter( e => {
	return !e.gratis
}).map(e=> {
	return e.producto.precio*e.cantidad
}).reduce((e,t)=>{
	return e+t 
})


// lineas= [{},{},{},{},{}{}]
// 			|filter|    // se filta los q no son gratuitos
// 		  [{},{},{},{}]
// 			 |map|	    // precio*cantidad
// 		  [20,30,40,60]
// 			 |reduce|  // suma todos los subtotales
// 			  150