import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { combineLatest } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'producto-form',
    templateUrl: '/form.cpt.html'
})
export class ProductoFormCtp {

    formulario: FormGroup;

    constructor(
        private http: HttpClient) {
    }

    ngOnInit() {
        this.formulario = new FormGroup({
            descripcion: new FormControl(null, [Validators.required]),
            precio: new FormControl(null, [Validators.required]),
            tipoAfectacion: new FormControl(null, [Validators.required]),
            precio2: new FormControl(null),
        });

        this.formulario.controls.precio2.disable()


        combineLatest(this.formulario.controls.precio.valueChanges,
            this.formulario.controls.tipoAfectacion.valueChanges)
            .subscribe((valores: any[]) => {
                console.log('valores')
                console.log(valores);
                const precio: number = valores[0];
                const tipoAfectacion: string = valores[1];
                const igv: number = tipoAfectacion === 'GRAVADO' ? 18 : 0;
                const precio2 = precio * (1 + igv / 100);

                this.formulario.controls.precio2.setValue(precio2)
                // var precio=valores[0]
            })


    }

    save() {
        if (this.formulario.valid) {
            alert('Formulario Valido');
            this.http
                .post('http://192.168.1.35:8080/api/productos',
                    this.formulario.value
                ).subscribe()
            //POST
        } else {
            alert('Formulario Invalido');
        }
    }



}

