import { Component } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { filter, debounceTime, flatMap } from 'rxjs/operators';

@Component({
    selector: 'producto-search',
    templateUrl: './search.html'
})
export class ProductoSearchCpt {

    productos = [];
    filterForm: FormGroup;
    constructor(
        private http: HttpClient
    ) {

    }

    ngOnInit() {
        this.filterForm = new FormGroup({
            descripcion: new FormControl()
        })
        this.filterForm.controls.descripcion.valueChanges
            .pipe(
                filter(descripcion => descripcion.length > 2),
                debounceTime(2000),
                flatMap(d => {

                    return this.http.get('http://192.168.1.35:8080/api/productos?d=' + d)
                })
            )
            .subscribe((resp: any) => {
                this.productos = resp.content;


            })

        // alert('nsgOnInit')
        this.http.get('http://192.168.1.35:8080/api/productos')
            .subscribe((resp: any) => {
                this.productos = resp.content;
            })
    }

    buscar() {
        const d = this.filterForm.controls.descripcion.value;
        this.http.get('http://192.168.1.35:8080/api/productos?d=' + d)
            .subscribe((resp: any) => {
                this.productos = resp.content;
            })

    }
}