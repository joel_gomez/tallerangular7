import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
// shift + alt +o 
// shit + alt +f --> indentar
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  formulario: FormGroup;
  ngOnInit(
  ) {


    this.formulario = new FormGroup({
      nombre: new FormControl(),
      apellido: new FormControl(),
      fechaNacimiento: new FormControl(),
      direccion: new FormGroup({
        fiscal: new FormControl(),
        domicilio: new FormControl(),
      })
    });

    this.formulario.controls.nombre.valueChanges
      .subscribe(nombre => {
        console.log('nombre:  ' + nombre)
        if (nombre == 'pepe') {
          this.formulario.controls.apellido.disable();
        } else {
          this.formulario.controls.apellido.enable();
        }
      })

  }



}
