import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HeaderCpt } from './header/header.cpt';
import { MenuCpt } from './menu/menu.cpt';
import { ProductoFormCtp } from './producto/form/form.cpt';
import { ProductoSearchCpt } from './producto/search/search.cpt';
import { VentaFormCtp } from './ventas/form/form.cpt';
import { RouterModule } from '@angular/router';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderCpt,
    MenuCpt,
    ProductoFormCtp,
    ProductoSearchCpt,

    VentaFormCtp,

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot([
      {
        path: 'productos',
        component: ProductoFormCtp
      }, {
        path: 'ventas',
        component: VentaFormCtp
      }
    ])
    // NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
